//
//  DatePickerViewController.m
//  Learning0516Pickers
//
//  日期选取器
//
//  Created by  apple on 13-5-16.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import "DatePickerViewController.h"

@interface DatePickerViewController ()

@end

@implementation DatePickerViewController
@synthesize datePicker;

-(void)dealloc{
    [datePicker release];
    [super dealloc];
}

-(void)buttonPressed:(id)sender{
    //获取选中的时间
    NSDate *selected = [datePicker date];
    NSString *message = [[NSString alloc] initWithFormat:
                         @"The date and tiem you selected is : %@", selected];
    
    //以警告的形式，提示用户选中的时间
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Date and time selected"
                                                     message:message
                                                    delegate:nil
                                           cancelButtonTitle:@"Yes, I did!"
                                           otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
    
    [message release];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // 初始化， 默认显示当前时间
    NSDate *now = [[NSDate alloc] init];
    [datePicker setDate:now  animated:NO];
    [now release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
