//
//  CustomPickerViewController.m
//  Learning0516Pickers
//  老虎机游戏
//  Created by  apple on 13-5-16.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import "CustomPickerViewController.h"

@interface CustomPickerViewController ()

@end

@implementation CustomPickerViewController
@synthesize picker;
@synthesize winLabel;
@synthesize column1;
@synthesize column2;
@synthesize column3;

@synthesize button;
@synthesize crunchSoundId;
@synthesize winSoundId;

-(void)showButton{
    button.hidden = NO;
}

-(void)playWinSound{
    //播放声音
    AudioServicesPlaySystemSound(winSoundId);
    winLabel.text = @"WIN!!!";
    //延迟1.5s， 显示spin按钮
    [self performSelector:@selector(showButton) withObject:nil afterDelay:1.5];
}

-(void)dealloc{
    [picker release];
    [winLabel release];
    [column1 release];
    [column2 release];
    [column3 release];
    
    [button release];
    
    //卸载声音
    if(winSoundId){
        AudioServicesDisposeSystemSoundID(winSoundId), winSoundId = 0;
    }
    if (crunchSoundId) {
        AudioServicesDisposeSystemSoundID(crunchSoundId), crunchSoundId = 0;
    }
    [super dealloc];
}

-(void)spin{
    //结果未出来之前，不允许再次点击
    button.hidden = YES;
    //播放转动时的声音
    AudioServicesPlaySystemSound(crunchSoundId);
    
    BOOL win = NO;
    //中标个数
    int numInRow = 1;
    //上次选中的下标
    int lastVal = -1;
    for (int i = 0; i < 3; i ++ ) {
        int newValue = random() % [self.column1 count];
        //选中相同，numInRow加1，否则，重置为1
        if (newValue == lastVal) {
            numInRow++;
        }else{
            numInRow = 1;
        }
        lastVal = newValue;
        //动态刷新
        [picker selectRow:newValue inComponent:i animated:YES];
        [picker reloadComponent:i];
        
        //3个都选中相同的，就赢了
        if(numInRow >= 3){
            win = YES;
        }
    }
    if(win){
        //赢了，就播放赢的声音
        //afterDelay预留了0.5秒的时间，让选取器的转动停下来，否则还没转停，结果就出来了
        [self performSelector:@selector(playWinSound)
                   withObject:nil afterDelay:.5];
    }else{
        //没赢，就显示按钮，可以再来一次
        [self performSelector:@selector(showButton)
                   withObject:nil afterDelay:.5];
    }
    winLabel.text = @"";
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIImage *img1 = [UIImage imageNamed:@"qg.png"];
    UIImage *img2 = [UIImage imageNamed:@"newest.png"];
    UIImage *img3 = [UIImage imageNamed:@"hot.png"];
    UIImage *img4 = [UIImage imageNamed:@"gx.png"];
    UIImage *img5 = [UIImage imageNamed:@"cy.png"];
    for(int i = 1 ; i <=3; i ++ ){
        UIImageView *img1View = [[UIImageView alloc] initWithImage:img1];
        UIImageView *img2View = [[UIImageView alloc] initWithImage:img2];
        UIImageView *img3View = [[UIImageView alloc] initWithImage:img3];
        UIImageView *img4View = [[UIImageView alloc] initWithImage:img4];
        UIImageView *img5View = [[UIImageView alloc] initWithImage:img5];
        img1View.frame = img2View.frame = img3View.frame = img4View.frame = img5View.frame = CGRectMake(0, 0, 40, 40);
        
        
        NSArray *imgViewArray = [[NSArray alloc] initWithObjects:img1View, img2View, img3View, img4View, img5View, nil];
        NSString *fieldName = [[NSString alloc] initWithFormat:@"column%d", i];
        [self setValue:imgViewArray forKey:fieldName];
        
        [fieldName release];
        [imgViewArray release];
        [img1View release];
        [img2View release];
        [img3View release];
        [img4View release];
        [img5View release];
        
    }
    //加载声音
    NSString * path = [[NSBundle mainBundle] pathForResource:@"win" ofType:@"wav"];
    AudioServicesCreateSystemSoundID((CFURLRef)[NSURL fileURLWithPath:path], &winSoundId);
    
    path = [[NSBundle mainBundle] pathForResource:@"crunch" ofType:@"wav"];
    AudioServicesCreateSystemSoundID((CFURLRef)[NSURL fileURLWithPath:path], &crunchSoundId);
    
    srandom(time(NULL));
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 
#pragma mark Picker Data Source Methods
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
    
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [self.column1 count]; //每个组件的count是一样的
}


#pragma mark Picker Delegate Methods
//这里是viewForRow,而不是titleForRow
//reusingView不能少
-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    NSString *arrayname = [[NSString alloc] initWithFormat:@"column%d", component + 1];
    NSArray *content = [self valueForKey:arrayname];
    UIImageView *selected = [content objectAtIndex:row];
    [arrayname release];
    return selected;
}

@end
