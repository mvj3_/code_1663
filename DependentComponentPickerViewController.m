//
//  DependentComponentPickerViewController.m
//  Learning0516Pickers
//  依赖组件
//  Created by  apple on 13-5-16.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import "DependentComponentPickerViewController.h"

@interface DependentComponentPickerViewController ()

@end

@implementation DependentComponentPickerViewController

@synthesize picker;
@synthesize stateZips;
@synthesize states;
@synthesize zips;

-(void)dealloc{
    [picker release];
    [stateZips release];
    [states release];
    [zips release];
    [super dealloc];
}

-(void)buttonPressed:(id)sender{
    //获取下标
    NSInteger stateRow = [picker selectedRowInComponent:kStateComponent];
    NSInteger zipRow = [picker selectedRowInComponent:kZipComponent];
    //获取值
    NSString * state = [states objectAtIndex:stateRow];
    NSString * zip = [zips objectAtIndex:zipRow];
    //构造提示信息
    NSString * title = [[NSString alloc] initWithFormat:@"You selected zip code %@", zip];
    NSString * message = [[NSString alloc] initWithFormat:@"%@ is in %@", zip, state];
    //提示
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
    [message release];
    [title release];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // 应用程序的主束， NSBundle作用： 获取添加到项目的Resources文件夹的资源
    NSBundle *bundle = [NSBundle mainBundle];
    //获取statedictionary.plist的文件路径
    NSString *plistPath = [bundle pathForResource: @"statedictionary" ofType:@"plist"];
    //根据路径，获取文件内容
    NSDictionary *dictionary = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    //赋值
    self.stateZips = dictionary;
    [dictionary release];
    
    //获取字典的所有key值
    NSArray *components = [self.stateZips allKeys];
    //排序
    NSArray *sorted = [components sortedArrayUsingSelector:@selector(compare:)];
    //赋值给state
    self.states = sorted;
    
    //默认选中第0个state
    NSString *selectedState = [self.states objectAtIndex:0];
    //获取选中state对应的zips
    NSArray *array = [stateZips objectForKey:selectedState];
    //赋值
    self.zips = array;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Picker Data Source Methods
//选取器组件个数
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

//每个组件 包含的行数
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(component == kStateComponent){
        return [self.states count];
    }
    return [self.zips count];
}

#pragma mark Picker Delegate Methods

//获取指定组件、指定行的字符串
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == kStateComponent) {
        return [self.states objectAtIndex:row];
    }
    return [self.zips objectAtIndex:row];
}

//选中时，触发的事件
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(component == kStateComponent){
        //获取选中的state
        NSString *selectedState = [self.states objectAtIndex:row];
        //根据选中的state， 获取其zips
        NSArray *array = [stateZips objectForKey:selectedState];
        //赋值
        self.zips = array;
        //还原zip选择器的位置，默认选择第0个
        [pickerView selectRow:0 inComponent:kZipComponent animated:YES];
        //重新加载zip选择器
        [picker reloadComponent:kZipComponent];
    }
}
@end
